import numpy as np
import pickle
from os.path import join, isdir
from os import listdir
import matplotlib.pyplot as plt
from keras.utils import Sequence


class DataGenerator(Sequence):
    def __init__(self, x, y, batch_size: int):
        self.x = x
        self.y = y
        self.shape_w, self.shape_h, self.layers = x[0].shape
        self.batch_size = batch_size
        self.unique_labels = np.unique(self.y)
        self.dict_unique_labels_to_idx = self.__get_dict_labels()

    def __get_dict_labels(self):
        res = {i: [] for i in self.unique_labels}
        for i, label, in enumerate(self.y):
            res[label].append(i)
        return res

    def __get_similar_pair(self):
        label = np.random.choice(self.unique_labels)
        l, r = np.random.choice(self.dict_unique_labels_to_idx[label], 2, replace=False)
        return l, r, 1

    def __get_dissimilar_pair(self):
        label_l, label_r = np.random.choice(self.unique_labels, 2, replace=False)
        l = np.random.choice(self.dict_unique_labels_to_idx[label_l])
        r = np.random.choice(self.dict_unique_labels_to_idx[label_r])
        return l, r, 0

    def get_pairs(self):
        pairs = []
        labels = []
        for i in range(len(self.x) * 2):
            idx1, idx2, _ = self.__get_similar_pair()
            pairs += [[self.x[idx1, :], self.x[idx2, :]]]
            idx1, idx2, _ = self.__get_dissimilar_pair()
            pairs += [[self.x[idx1, :], self.x[idx2, :]]]
            labels += [1, 0]
        return np.array(pairs), np.array(labels)

    def __get_batch(self):
        pairs = []
        labels = []
        for i in range(self.batch_size):
            idx1, idx2, _ = self.__get_similar_pair()
            pairs += [[self.x[idx1, :], self.x[idx2, :]]]
            idx1, idx2, _ = self.__get_dissimilar_pair()
            pairs += [[self.x[idx1, :], self.x[idx2, :]]]
            labels += [1, 0]
        return np.array(pairs), np.array(labels)

    def __getitem__(self, index):
        pairs, labels = self.__get_batch()
        return [pairs[:, 0], pairs[:, 1]], labels

    def __len__(self):
        return int(np.floor(self.x.shape[0] / self.batch_size))


def load_dir(path):
    x, y = [], []
    curr_class = 0
    languages = [join(path, dir) for dir in listdir(path) if isdir(join(path, dir))]

    for language_path in languages:
        print("Loading {}".format(language_path.split('\\')[-1]))
        symb_dirs = [join(language_path, dir) for dir in listdir(language_path) if isdir(join(language_path, dir))]
        for symb_dir in symb_dirs:
            for img_file in listdir(symb_dir):
                fullpath = join(language_path, symb_dir, img_file).replace('\\', '/')
                x.append(plt.imread(fullpath))
                y.append(curr_class)
            curr_class += 1
    x = np.array(x)
    y = np.array(y)
    els, h, w = x.shape
    x = x.reshape((els, h, w))
    print("\tEntities loaded: {}".format(x.shape[0]))
    return x, y


def load_pickle(path):
    x, y = [], []
    with open(path, 'rb') as file:
        data = pickle.load(file)
    x, y = data
    return x, y


def preprocess_imgs(data, n_layers=1):
    data = data.astype('float32')
    data /= 255
    data = np.reshape(data, data.shape + tuple([n_layers]))
    return data


