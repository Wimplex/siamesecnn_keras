import os
import time
import sys
import datetime
import matplotlib.pyplot as plt
from config import config
from data_processer import DataGenerator, load_pickle, preprocess_imgs
import keras.backend as K
from keras.models import Sequential, Model
from keras.layers import Conv2D, MaxPooling2D, Lambda, Flatten, Input, Dropout, Dense
from keras.optimizers import RMSprop, Adam
from keras.regularizers import l2
from keras.initializers import RandomNormal


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
now = datetime.datetime.now()


def compute_accuracy(y_true, y_pred):
    import numpy as np
    pred = y_pred.ravel() < 0.5
    return np.mean(pred == y_true)


def plot_history(history):
    plt.rcParams['figure.figsize'] = 16, 9
    colors = ['r', 'g', 'b', 'y']
    for i, item in enumerate(history.history.keys()):
        epochs = len(history.history[item])
        plt.subplot(2, 2, i + 1)
        plt.plot(range(epochs), history.history[item], color=colors[i])
        plt.title(item)
    try:
        f_name = "history_{}".format(now.strftime('%d_%m_%Y_%H_%M'))
        plt.savefig(os.path.join(config.path_plot_dir, f_name))
    except Exception as e:
        print('Plot: ', e)
        time.sleep(10)
        sys.exit()


def prepare_data(return_type='list'):
    print("Loading data")
    train_x, train_y = load_pickle(config.path_train_pickle)
    val_x, val_y = load_pickle(config.path_val_pickle)

    print("Preprocessing data")
    train_x = preprocess_imgs(train_x)
    val_x = preprocess_imgs(val_x)
    val_x = val_x[:100, ]
    val_y = val_y[:100, ]

    print("Compiling pairs")
    train_generator = DataGenerator(train_x, train_y, config.batch_size)
    val_generator = DataGenerator(val_x, val_y, config.batch_size)
    if return_type == 'generator':
        return train_generator, val_generator
    else:
        train_pairs, train_labels = train_generator.get_pairs()
        val_pairs, val_labels = val_generator.get_pairs()
        return (train_pairs, train_labels), (val_pairs, val_labels)


class SiameseNetwork:
    def __init__(self):
        self.siamese_net = None

    def compile(self, verbose=1):
        inp_a = Input(config.input_shape)
        inp_b = Input(config.input_shape)
        cnn = Sequential()
        cnn.add(Conv2D(16, 7,
                       activation='relu',
                       input_shape=config.input_shape,
                       data_format='channels_last',
                       kernel_regularizer=l2(2e-3),
                       kernel_initializer=RandomNormal(0, 0.05,
                                                       seed=datetime.datetime.now().microsecond)))
        cnn.add(MaxPooling2D())
        cnn.add(Conv2D(16, 7,
                       activation='relu',
                       kernel_regularizer=l2(2e-3),
                       kernel_initializer=RandomNormal(0, 0.05,
                                                       seed=datetime.datetime.now().microsecond)))
        cnn.add(MaxPooling2D())
        cnn.add(Conv2D(32, 5,
                       activation='relu',
                       kernel_regularizer=l2(2e-3),
                       kernel_initializer=RandomNormal(0, 0.05,
                                                       seed=datetime.datetime.now().microsecond)))
        #cnn.add(MaxPooling2D())

        #cnn.add(Conv2D(2, (1, 1),
        #               activation=None,
        #               kernel_initializer=RandomNormal(0, 0.05,
        #                                               seed=datetime.datetime.now().microsecond)))
        #cnn.add(MaxPooling2D())
        cnn.add(Flatten())
        cnn.add(Dense(18, activation='sigmoid'))

        outp_a = cnn(inp_a)
        outp_b = cnn(inp_b)
        distance = Lambda(self.__euclidean_distance,
                          output_shape=self.__eucl_dist_output_shape)([outp_a, outp_b])
        self.siamese_net = Model(inputs=[inp_a, inp_b], outputs=distance)
        self.siamese_net.compile(loss=self.__contrastive_loss,
                            #optimizer=RMSprop(lr=6e-5),
                            optimizer=Adam(0.001, decay=2.5e-4),
                            metrics=[self.accuracy])
        if verbose == 1:
            print('Params count: ', self.siamese_net.count_params())

    def fit(self, train_x, train_y, batch_size, epochs, val_x, val_y):
        self.history = self.siamese_net.fit(
            [train_x[:, 0], train_x[:, 1]], train_y,
            batch_size=batch_size,
            epochs=epochs,
            validation_data=([val_x[:, 0], val_x[:, 1]], val_y),
            verbose=1
        )
        return self.history

    def save(self, path):
        f_name = "model_{}_({}%).h5".format(
            now.strftime('%d_%m_%Y_%H_%M'),
            int(100 * self.history.history['val_accuracy'][-1])
        )
        try:
            self.siamese_net.save(os.path.join(path, f_name))
        except MemoryError as e:
            print('Memory error: ', e)
        except KeyError as e:
            print('Key error: ', e)
        finally:
            time.sleep(10)
            sys.exit(1)

    @staticmethod
    def __contrastive_loss(y_true, y_pred):
        margin = 1
        square_pred = K.square(y_pred)
        margin_square = K.square(K.maximum(margin - y_pred, 0))
        return K.mean(y_true * square_pred + (1 - y_true) * margin_square)

    @staticmethod
    def __euclidean_distance(x):
        x1, x2 = x
        sum_square = K.sum(K.square(x1 - x2), axis=1, keepdims=True)
        return K.sqrt(K.maximum(sum_square, K.epsilon()))

    @staticmethod
    def __eucl_dist_output_shape(shapes):
        shape1, shape2 = shapes
        return shape1[0], 1

    @staticmethod
    def accuracy(y_true, y_pred):
        return K.mean(K.equal(y_true, K.cast(y_pred < 0.5, y_true.dtype)))


(train_pairs, train_labels), (val_pairs, val_labels) = prepare_data(return_type='list')
nn = SiameseNetwork()
nn.compile()
history = nn.fit(
    train_x=train_pairs,
    train_y=train_labels,
    batch_size=config.batch_size,
    epochs=config.epochs,
    val_x=val_pairs,
    val_y=val_labels
)
nn.save(config.path_models_dir)
plot_history(history)

