class Config:
    def __init__(self):
        # Train parameters
        self.batch_size = 128
        self.epochs = 30
        self.train_steps_per_epoch = 64
        self.val_steps_per_epoch = 64,
        self.input_shape = (105, 105, 1)

        # For using 'load_dir'
        self.path_train_dir = r"C:\Users\Alexey\Documents\Datasets\Omniglot\images_background"
        self.path_val_dir = r"C:\Users\Alexey\Documents\Datasets\Omniglot\images_evaluation"

        # For using 'load_pickle'
        self.path_train_pickle = "dataset/train.pickle"
        self.path_val_pickle = "dataset/val.pickle"

        # Other
        self.path_plot_dir = "plots/"
        self.path_models_dir = "models/"


config = Config()